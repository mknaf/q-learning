#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

import random
import settings

from collections import defaultdict

from Environment import Environment


class Agent:

    def __init__(self, actions=settings.action_space):
        self.actions = actions
        self.learning_rate = settings.learning_rate
        self.discount_factor = settings.discount_factor
        self.epsilon = settings.epsilon
        self.q_table = defaultdict(
            lambda: [0.0 for i in range(len(self.actions))]
        )
        self.credit = 0.0

    def _get_next_action_epsilon_greedy(self, state):
        """
        Return an action for a given state chosen by
        epsilon-greedy policy.
        """
        # take a random action aka explore
        if random.random() < self.epsilon:
            return random.choice(self.actions)
        # take the best action aka exploit
        else:
            return self._arg_max(self.q_table[state])

    def _arg_max(self, action_values):
        """
        Returns the best of all given actions, or if there are equally good
        choices, returns a random choice of the highest valued actions.
        """
        max_index_list = []
        max_value = 0.0

        # for all actions
        for i, v in enumerate(action_values):
            # if the current action's value is higher than
            # the current max value
            if v > max_value:

                # drop all previous max values
                max_index_list.clear()

                # remember the new max value
                max_value = v

                # append the current action to the
                # list of maximum value indices
                max_index_list.append(i)

            # elif the current value eqals the max value
            elif v == max_value:

                # append the current action to the
                # list of maximum value indices
                max_index_list.append(i)

        # return a random action from the max actions
        return random.choice(max_index_list)

    def _learn(self, state, action, reward, next_state):
        """
        Update the value of an action in the q_table
        using a state, an action, a reward and the next_state.
        """
        current_q = self.q_table[state][action]
        new_q = reward + self.discount_factor + max(self.q_table[next_state])
        self.q_table[state][action] += self.learning_rate * (new_q - current_q)

        self.credit += reward


if __name__ == "__main__":

    # a place to store the results of all the runs
    data = {}

    for episode in range(settings.episode_step, settings.episodes, settings.episode_step):
        env = Environment()
        agent = Agent(actions=settings.action_space)

        agent.credit = 0.0

        for step in range(episode):

            # reset the environment to the start state
            env.reset()

            # find out which state the Environment is in
            state = env.cur_state

            while True:
                # make a move depending on the current state
                action = agent._get_next_action_epsilon_greedy(state)
                next_state, reward, done = env.play(action)

                # with sample <s, a, r, s'>, learn new q function
                agent._learn(state, action, reward, next_state)

                state = next_state

                # if we reached a final state
                if done:
                    break

        data[episode] = [agent.credit, settings.A["bias"], settings.B["bias"]]


    df = pd.DataFrame(data, index=["credit", "bias A", "bias B"])
    print(df)

