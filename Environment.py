#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import settings


class Environment:

    def __init__(self):
        self.cur_state = 0

    def _play_a(self):
        # roll against our bias
        if random.random() < settings.A["bias"]:  # player wins
            return settings.A["gain"]
        else:  # player loses
            return settings.A["loss"]

    def _play_b(self):
        if random.random() < settings.B["bias"]:  # player wins
            return settings.B["gain"]
        else:  # player loses
            return settings.B["loss"]

    def reset(self):
        self.cur_state = 0

    def play(self, action):

        if action in [0, 1]:  # automaton A
            next_state = 1
            if action == 0:  # .5 €
                reward = .5 + self._play_a()
            else:  # 1€
                reward = 1 + self._play_a()

        elif action in [2, 3]:  # automaton B
            next_state = 2
            if action == 2:  # .5 €
                reward = .5 + self._play_b()
            else:  # 1 €
                reward = 1 + self._play_b()

        # we have no more states and we're done
        done = True

        return next_state, reward, done
