#!/usr/bin/env python
# -*- coding: utf-8 -*-

learning_rate = 0.01
discount_factor = 0.9
epsilon = 0.1
episodes = 1000  # maximum episodes
episode_step = 50  # step to maximum episodes in this rate (50, 100, 150, ...)


# possible actions
action_space = list(range(4))

# automata
A = {
    "bias": .5,  # bias towards gain
    "gain": .05,
    "loss": -.05
}

B = {
    "bias": 1 / 3,  # bias towards gain
    "gain": 2.5,
    "loss": -1
}

